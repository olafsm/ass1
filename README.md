# Ecore model of Discgolfmetrix.com
## The website
Discgolfmetrix is a website for tracking and organizing discgolf events. Users sign up and can join clubs, join events, host events, play practice rounds, track their scores and much more. 
I will model a small part of it including the core concepts such as players, an event, a club and a disc golf course.

[![N|Solid](https://i.gyazo.com/b924c041117d2d3b4210b6c4bb860313.png)](https://i.gyazo.com/b924c041117d2d3b4210b6c4bb860313.png)
[![N|Solid](https://i.gyazo.com/e0edd766a43bd686531ce561e388fc50.png)](https://i.gyazo.com/e0edd766a43bd686531ce561e388fc50.png)
[![N|Solid](https://i.gyazo.com/4a5ae7e19e6a18a8aaa95558549afffd.png)](https://i.gyazo.com/4a5ae7e19e6a18a8aaa95558549afffd.png)



## The model
The root element is the Discgolfmetrix which acts as the **container** for the rest of the website. The top level concepts within the website are as mentioned the User, the Club, the Course and the Events. Course can have multiple layouts, where each layout usually consist of 9 or 18 holes. Each hole is defined by its distance and their par. The par is an **Enum**(range 2-7), while distance is a **derived feature** where you can provide either meters or feet, and the other one will be converted automatically. The users name is also a derived feature, derived from the **attributes** firstname and lastname. An example of **opposite** is Course and CourseLayout, each layout can only belong to one course, while the same course can have multiple layouts.

There is a **constraint** on User and Event. One user cannot be both a participant and coordinator of the same event.
[![N|Solid](https://i.gyazo.com/8720234a6c9860cebfa58eef2a9a78f4.png)](https://i.gyazo.com/8720234a6c9860cebfa58eef2a9a78f4.png)


## An instance
While the specific data in the instance doesnt match the text on the website, the model is generally representative of the core functionality of participating in an event. 
[![N|Solid](https://i.gyazo.com/d76fe4150b4517fb013ead21ae4435f0.png)](https://i.gyazo.com/d76fe4150b4517fb013ead21ae4435f0.png)
[![N|Solid](https://i.gyazo.com/8600c727391f514713ad21fd12765b1c.png)](https://i.gyazo.com/8600c727391f514713ad21fd12765b1c.png)

## Ecore Concepts
### Manual implementation of constraint
[![N|Solid](https://i.gyazo.com/b6a4bd23ebf5884883d5c6604f0bdc04.png)](https://i.gyazo.com/b6a4bd23ebf5884883d5c6604f0bdc04.png)
### Manual implementation of one derived feature
[![N|Solid](https://i.gyazo.com/2ae0b56d3c4c22df614a36d0c58db825.png)](https://i.gyazo.com/2ae0b56d3c4c22df614a36d0c58db825.png)