/**
 */
package tdt4250.discgolfmetrix;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hole</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.discgolfmetrix.Hole#getPar <em>Par</em>}</li>
 *   <li>{@link tdt4250.discgolfmetrix.Hole#getHoleNumber <em>Hole Number</em>}</li>
 *   <li>{@link tdt4250.discgolfmetrix.Hole#getCourseLayout <em>Course Layout</em>}</li>
 *   <li>{@link tdt4250.discgolfmetrix.Hole#getDistanceInMeters <em>Distance In Meters</em>}</li>
 *   <li>{@link tdt4250.discgolfmetrix.Hole#getDistanceInFeet <em>Distance In Feet</em>}</li>
 * </ul>
 *
 * @see tdt4250.discgolfmetrix.DiscgolfmetrixPackage#getHole()
 * @model
 * @generated
 */
public interface Hole extends EObject {
	/**
	 * Returns the value of the '<em><b>Par</b></em>' attribute.
	 * The literals are from the enumeration {@link tdt4250.discgolfmetrix.Par}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Par</em>' attribute.
	 * @see tdt4250.discgolfmetrix.Par
	 * @see #setPar(Par)
	 * @see tdt4250.discgolfmetrix.DiscgolfmetrixPackage#getHole_Par()
	 * @model unique="false" required="true"
	 * @generated
	 */
	Par getPar();

	/**
	 * Sets the value of the '{@link tdt4250.discgolfmetrix.Hole#getPar <em>Par</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Par</em>' attribute.
	 * @see tdt4250.discgolfmetrix.Par
	 * @see #getPar()
	 * @generated
	 */
	void setPar(Par value);

	/**
	 * Returns the value of the '<em><b>Hole Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hole Number</em>' attribute.
	 * @see #setHoleNumber(Integer)
	 * @see tdt4250.discgolfmetrix.DiscgolfmetrixPackage#getHole_HoleNumber()
	 * @model unique="false" required="true"
	 * @generated
	 */
	Integer getHoleNumber();

	/**
	 * Sets the value of the '{@link tdt4250.discgolfmetrix.Hole#getHoleNumber <em>Hole Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hole Number</em>' attribute.
	 * @see #getHoleNumber()
	 * @generated
	 */
	void setHoleNumber(Integer value);

	/**
	 * Returns the value of the '<em><b>Course Layout</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link tdt4250.discgolfmetrix.CourseLayout#getHole <em>Hole</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Course Layout</em>' container reference.
	 * @see #setCourseLayout(CourseLayout)
	 * @see tdt4250.discgolfmetrix.DiscgolfmetrixPackage#getHole_CourseLayout()
	 * @see tdt4250.discgolfmetrix.CourseLayout#getHole
	 * @model opposite="hole" required="true" transient="false"
	 * @generated
	 */
	CourseLayout getCourseLayout();

	/**
	 * Sets the value of the '{@link tdt4250.discgolfmetrix.Hole#getCourseLayout <em>Course Layout</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Course Layout</em>' container reference.
	 * @see #getCourseLayout()
	 * @generated
	 */
	void setCourseLayout(CourseLayout value);

	/**
	 * Returns the value of the '<em><b>Distance In Meters</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Distance In Meters</em>' attribute.
	 * @see #setDistanceInMeters(Integer)
	 * @see tdt4250.discgolfmetrix.DiscgolfmetrixPackage#getHole_DistanceInMeters()
	 * @model unique="false" required="true" derived="true"
	 * @generated
	 */
	Integer getDistanceInMeters();

	/**
	 * Sets the value of the '{@link tdt4250.discgolfmetrix.Hole#getDistanceInMeters <em>Distance In Meters</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Distance In Meters</em>' attribute.
	 * @see #getDistanceInMeters()
	 * @generated
	 */
	void setDistanceInMeters(Integer value);

	/**
	 * Returns the value of the '<em><b>Distance In Feet</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Distance In Feet</em>' attribute.
	 * @see #setDistanceInFeet(Integer)
	 * @see tdt4250.discgolfmetrix.DiscgolfmetrixPackage#getHole_DistanceInFeet()
	 * @model unique="false" required="true" derived="true"
	 * @generated
	 */
	Integer getDistanceInFeet();

	/**
	 * Sets the value of the '{@link tdt4250.discgolfmetrix.Hole#getDistanceInFeet <em>Distance In Feet</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Distance In Feet</em>' attribute.
	 * @see #getDistanceInFeet()
	 * @generated
	 */
	void setDistanceInFeet(Integer value);

} // Hole
