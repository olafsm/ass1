/**
 */
package tdt4250.discgolfmetrix;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Course Layout</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.discgolfmetrix.CourseLayout#getCourse <em>Course</em>}</li>
 *   <li>{@link tdt4250.discgolfmetrix.CourseLayout#getHole <em>Hole</em>}</li>
 *   <li>{@link tdt4250.discgolfmetrix.CourseLayout#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see tdt4250.discgolfmetrix.DiscgolfmetrixPackage#getCourseLayout()
 * @model
 * @generated
 */
public interface CourseLayout extends EObject {
	/**
	 * Returns the value of the '<em><b>Course</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link tdt4250.discgolfmetrix.Course#getLayouts <em>Layouts</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Course</em>' container reference.
	 * @see #setCourse(Course)
	 * @see tdt4250.discgolfmetrix.DiscgolfmetrixPackage#getCourseLayout_Course()
	 * @see tdt4250.discgolfmetrix.Course#getLayouts
	 * @model opposite="layouts" required="true" transient="false"
	 * @generated
	 */
	Course getCourse();

	/**
	 * Sets the value of the '{@link tdt4250.discgolfmetrix.CourseLayout#getCourse <em>Course</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Course</em>' container reference.
	 * @see #getCourse()
	 * @generated
	 */
	void setCourse(Course value);

	/**
	 * Returns the value of the '<em><b>Hole</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250.discgolfmetrix.Hole}.
	 * It is bidirectional and its opposite is '{@link tdt4250.discgolfmetrix.Hole#getCourseLayout <em>Course Layout</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hole</em>' containment reference list.
	 * @see tdt4250.discgolfmetrix.DiscgolfmetrixPackage#getCourseLayout_Hole()
	 * @see tdt4250.discgolfmetrix.Hole#getCourseLayout
	 * @model opposite="courseLayout" containment="true" required="true"
	 * @generated
	 */
	EList<Hole> getHole();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see tdt4250.discgolfmetrix.DiscgolfmetrixPackage#getCourseLayout_Name()
	 * @model id="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link tdt4250.discgolfmetrix.CourseLayout#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

} // CourseLayout
