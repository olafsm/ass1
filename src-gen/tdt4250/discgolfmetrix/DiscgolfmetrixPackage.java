/**
 */
package tdt4250.discgolfmetrix;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see tdt4250.discgolfmetrix.DiscgolfmetrixFactory
 * @model kind="package"
 * @generated
 */
public interface DiscgolfmetrixPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "discgolfmetrix";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.example.org/discgolfmetrix";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "discgolfmetrix";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DiscgolfmetrixPackage eINSTANCE = tdt4250.discgolfmetrix.impl.DiscgolfmetrixPackageImpl.init();

	/**
	 * The meta object id for the '{@link tdt4250.discgolfmetrix.impl.UserImpl <em>User</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.discgolfmetrix.impl.UserImpl
	 * @see tdt4250.discgolfmetrix.impl.DiscgolfmetrixPackageImpl#getUser()
	 * @generated
	 */
	int USER = 0;

	/**
	 * The feature id for the '<em><b>Firstname</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER__FIRSTNAME = 0;

	/**
	 * The feature id for the '<em><b>Lastname</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER__LASTNAME = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER__NAME = 2;

	/**
	 * The feature id for the '<em><b>Nickname</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER__NICKNAME = 3;

	/**
	 * The feature id for the '<em><b>Email</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER__EMAIL = 4;

	/**
	 * The feature id for the '<em><b>PDG Anumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER__PDG_ANUMBER = 5;

	/**
	 * The feature id for the '<em><b>User ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER__USER_ID = 6;

	/**
	 * The feature id for the '<em><b>Rating</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER__RATING = 7;

	/**
	 * The feature id for the '<em><b>Club</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER__CLUB = 8;

	/**
	 * The feature id for the '<em><b>Participant</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER__PARTICIPANT = 9;

	/**
	 * The feature id for the '<em><b>Coordinator</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER__COORDINATOR = 10;

	/**
	 * The number of structural features of the '<em>User</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_FEATURE_COUNT = 11;

	/**
	 * The number of operations of the '<em>User</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250.discgolfmetrix.impl.ClubImpl <em>Club</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.discgolfmetrix.impl.ClubImpl
	 * @see tdt4250.discgolfmetrix.impl.DiscgolfmetrixPackageImpl#getClub()
	 * @generated
	 */
	int CLUB = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUB__NAME = 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUB__DESCRIPTION = 1;

	/**
	 * The feature id for the '<em><b>Member</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUB__MEMBER = 2;

	/**
	 * The number of structural features of the '<em>Club</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUB_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Club</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUB_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250.discgolfmetrix.impl.CourseImpl <em>Course</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.discgolfmetrix.impl.CourseImpl
	 * @see tdt4250.discgolfmetrix.impl.DiscgolfmetrixPackageImpl#getCourse()
	 * @generated
	 */
	int COURSE = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Baskets</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__BASKETS = 1;

	/**
	 * The feature id for the '<em><b>Layouts</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__LAYOUTS = 2;

	/**
	 * The number of structural features of the '<em>Course</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Course</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250.discgolfmetrix.impl.CourseLayoutImpl <em>Course Layout</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.discgolfmetrix.impl.CourseLayoutImpl
	 * @see tdt4250.discgolfmetrix.impl.DiscgolfmetrixPackageImpl#getCourseLayout()
	 * @generated
	 */
	int COURSE_LAYOUT = 3;

	/**
	 * The feature id for the '<em><b>Course</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_LAYOUT__COURSE = 0;

	/**
	 * The feature id for the '<em><b>Hole</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_LAYOUT__HOLE = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_LAYOUT__NAME = 2;

	/**
	 * The number of structural features of the '<em>Course Layout</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_LAYOUT_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Course Layout</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_LAYOUT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250.discgolfmetrix.impl.HoleImpl <em>Hole</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.discgolfmetrix.impl.HoleImpl
	 * @see tdt4250.discgolfmetrix.impl.DiscgolfmetrixPackageImpl#getHole()
	 * @generated
	 */
	int HOLE = 4;

	/**
	 * The feature id for the '<em><b>Par</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOLE__PAR = 0;

	/**
	 * The feature id for the '<em><b>Hole Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOLE__HOLE_NUMBER = 1;

	/**
	 * The feature id for the '<em><b>Course Layout</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOLE__COURSE_LAYOUT = 2;

	/**
	 * The feature id for the '<em><b>Distance In Meters</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOLE__DISTANCE_IN_METERS = 3;

	/**
	 * The feature id for the '<em><b>Distance In Feet</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOLE__DISTANCE_IN_FEET = 4;

	/**
	 * The number of structural features of the '<em>Hole</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOLE_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Hole</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOLE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250.discgolfmetrix.impl.EventImpl <em>Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.discgolfmetrix.impl.EventImpl
	 * @see tdt4250.discgolfmetrix.impl.DiscgolfmetrixPackageImpl#getEvent()
	 * @generated
	 */
	int EVENT = 5;

	/**
	 * The feature id for the '<em><b>Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__START_TIME = 0;

	/**
	 * The feature id for the '<em><b>Course</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__COURSE = 1;

	/**
	 * The feature id for the '<em><b>Participant</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__PARTICIPANT = 2;

	/**
	 * The feature id for the '<em><b>Coordinator</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__COORDINATOR = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__NAME = 4;

	/**
	 * The number of structural features of the '<em>Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250.discgolfmetrix.impl.DiscgolfmetrixImpl <em>Discgolfmetrix</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.discgolfmetrix.impl.DiscgolfmetrixImpl
	 * @see tdt4250.discgolfmetrix.impl.DiscgolfmetrixPackageImpl#getDiscgolfmetrix()
	 * @generated
	 */
	int DISCGOLFMETRIX = 6;

	/**
	 * The feature id for the '<em><b>User</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISCGOLFMETRIX__USER = 0;

	/**
	 * The feature id for the '<em><b>Course</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISCGOLFMETRIX__COURSE = 1;

	/**
	 * The feature id for the '<em><b>Club</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISCGOLFMETRIX__CLUB = 2;

	/**
	 * The feature id for the '<em><b>Event</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISCGOLFMETRIX__EVENT = 3;

	/**
	 * The number of structural features of the '<em>Discgolfmetrix</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISCGOLFMETRIX_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Discgolfmetrix</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISCGOLFMETRIX_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250.discgolfmetrix.Par <em>Par</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.discgolfmetrix.Par
	 * @see tdt4250.discgolfmetrix.impl.DiscgolfmetrixPackageImpl#getPar()
	 * @generated
	 */
	int PAR = 7;

	/**
	 * Returns the meta object for class '{@link tdt4250.discgolfmetrix.User <em>User</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>User</em>'.
	 * @see tdt4250.discgolfmetrix.User
	 * @generated
	 */
	EClass getUser();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.discgolfmetrix.User#getFirstname <em>Firstname</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Firstname</em>'.
	 * @see tdt4250.discgolfmetrix.User#getFirstname()
	 * @see #getUser()
	 * @generated
	 */
	EAttribute getUser_Firstname();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.discgolfmetrix.User#getLastname <em>Lastname</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lastname</em>'.
	 * @see tdt4250.discgolfmetrix.User#getLastname()
	 * @see #getUser()
	 * @generated
	 */
	EAttribute getUser_Lastname();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.discgolfmetrix.User#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see tdt4250.discgolfmetrix.User#getName()
	 * @see #getUser()
	 * @generated
	 */
	EAttribute getUser_Name();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.discgolfmetrix.User#getNickname <em>Nickname</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Nickname</em>'.
	 * @see tdt4250.discgolfmetrix.User#getNickname()
	 * @see #getUser()
	 * @generated
	 */
	EAttribute getUser_Nickname();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.discgolfmetrix.User#getEmail <em>Email</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Email</em>'.
	 * @see tdt4250.discgolfmetrix.User#getEmail()
	 * @see #getUser()
	 * @generated
	 */
	EAttribute getUser_Email();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.discgolfmetrix.User#getPDGAnumber <em>PDG Anumber</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>PDG Anumber</em>'.
	 * @see tdt4250.discgolfmetrix.User#getPDGAnumber()
	 * @see #getUser()
	 * @generated
	 */
	EAttribute getUser_PDGAnumber();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.discgolfmetrix.User#getUserID <em>User ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>User ID</em>'.
	 * @see tdt4250.discgolfmetrix.User#getUserID()
	 * @see #getUser()
	 * @generated
	 */
	EAttribute getUser_UserID();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.discgolfmetrix.User#getRating <em>Rating</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Rating</em>'.
	 * @see tdt4250.discgolfmetrix.User#getRating()
	 * @see #getUser()
	 * @generated
	 */
	EAttribute getUser_Rating();

	/**
	 * Returns the meta object for the container reference '{@link tdt4250.discgolfmetrix.User#getClub <em>Club</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Club</em>'.
	 * @see tdt4250.discgolfmetrix.User#getClub()
	 * @see #getUser()
	 * @generated
	 */
	EReference getUser_Club();

	/**
	 * Returns the meta object for the reference list '{@link tdt4250.discgolfmetrix.User#getParticipant <em>Participant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Participant</em>'.
	 * @see tdt4250.discgolfmetrix.User#getParticipant()
	 * @see #getUser()
	 * @generated
	 */
	EReference getUser_Participant();

	/**
	 * Returns the meta object for the reference list '{@link tdt4250.discgolfmetrix.User#getCoordinator <em>Coordinator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Coordinator</em>'.
	 * @see tdt4250.discgolfmetrix.User#getCoordinator()
	 * @see #getUser()
	 * @generated
	 */
	EReference getUser_Coordinator();

	/**
	 * Returns the meta object for class '{@link tdt4250.discgolfmetrix.Club <em>Club</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Club</em>'.
	 * @see tdt4250.discgolfmetrix.Club
	 * @generated
	 */
	EClass getClub();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.discgolfmetrix.Club#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see tdt4250.discgolfmetrix.Club#getName()
	 * @see #getClub()
	 * @generated
	 */
	EAttribute getClub_Name();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.discgolfmetrix.Club#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see tdt4250.discgolfmetrix.Club#getDescription()
	 * @see #getClub()
	 * @generated
	 */
	EAttribute getClub_Description();

	/**
	 * Returns the meta object for the containment reference list '{@link tdt4250.discgolfmetrix.Club#getMember <em>Member</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Member</em>'.
	 * @see tdt4250.discgolfmetrix.Club#getMember()
	 * @see #getClub()
	 * @generated
	 */
	EReference getClub_Member();

	/**
	 * Returns the meta object for class '{@link tdt4250.discgolfmetrix.Course <em>Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Course</em>'.
	 * @see tdt4250.discgolfmetrix.Course
	 * @generated
	 */
	EClass getCourse();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.discgolfmetrix.Course#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see tdt4250.discgolfmetrix.Course#getName()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_Name();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.discgolfmetrix.Course#getBaskets <em>Baskets</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Baskets</em>'.
	 * @see tdt4250.discgolfmetrix.Course#getBaskets()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_Baskets();

	/**
	 * Returns the meta object for the containment reference list '{@link tdt4250.discgolfmetrix.Course#getLayouts <em>Layouts</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Layouts</em>'.
	 * @see tdt4250.discgolfmetrix.Course#getLayouts()
	 * @see #getCourse()
	 * @generated
	 */
	EReference getCourse_Layouts();

	/**
	 * Returns the meta object for class '{@link tdt4250.discgolfmetrix.CourseLayout <em>Course Layout</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Course Layout</em>'.
	 * @see tdt4250.discgolfmetrix.CourseLayout
	 * @generated
	 */
	EClass getCourseLayout();

	/**
	 * Returns the meta object for the container reference '{@link tdt4250.discgolfmetrix.CourseLayout#getCourse <em>Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Course</em>'.
	 * @see tdt4250.discgolfmetrix.CourseLayout#getCourse()
	 * @see #getCourseLayout()
	 * @generated
	 */
	EReference getCourseLayout_Course();

	/**
	 * Returns the meta object for the containment reference list '{@link tdt4250.discgolfmetrix.CourseLayout#getHole <em>Hole</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Hole</em>'.
	 * @see tdt4250.discgolfmetrix.CourseLayout#getHole()
	 * @see #getCourseLayout()
	 * @generated
	 */
	EReference getCourseLayout_Hole();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.discgolfmetrix.CourseLayout#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see tdt4250.discgolfmetrix.CourseLayout#getName()
	 * @see #getCourseLayout()
	 * @generated
	 */
	EAttribute getCourseLayout_Name();

	/**
	 * Returns the meta object for class '{@link tdt4250.discgolfmetrix.Hole <em>Hole</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Hole</em>'.
	 * @see tdt4250.discgolfmetrix.Hole
	 * @generated
	 */
	EClass getHole();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.discgolfmetrix.Hole#getPar <em>Par</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Par</em>'.
	 * @see tdt4250.discgolfmetrix.Hole#getPar()
	 * @see #getHole()
	 * @generated
	 */
	EAttribute getHole_Par();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.discgolfmetrix.Hole#getHoleNumber <em>Hole Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Hole Number</em>'.
	 * @see tdt4250.discgolfmetrix.Hole#getHoleNumber()
	 * @see #getHole()
	 * @generated
	 */
	EAttribute getHole_HoleNumber();

	/**
	 * Returns the meta object for the container reference '{@link tdt4250.discgolfmetrix.Hole#getCourseLayout <em>Course Layout</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Course Layout</em>'.
	 * @see tdt4250.discgolfmetrix.Hole#getCourseLayout()
	 * @see #getHole()
	 * @generated
	 */
	EReference getHole_CourseLayout();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.discgolfmetrix.Hole#getDistanceInMeters <em>Distance In Meters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Distance In Meters</em>'.
	 * @see tdt4250.discgolfmetrix.Hole#getDistanceInMeters()
	 * @see #getHole()
	 * @generated
	 */
	EAttribute getHole_DistanceInMeters();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.discgolfmetrix.Hole#getDistanceInFeet <em>Distance In Feet</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Distance In Feet</em>'.
	 * @see tdt4250.discgolfmetrix.Hole#getDistanceInFeet()
	 * @see #getHole()
	 * @generated
	 */
	EAttribute getHole_DistanceInFeet();

	/**
	 * Returns the meta object for class '{@link tdt4250.discgolfmetrix.Event <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event</em>'.
	 * @see tdt4250.discgolfmetrix.Event
	 * @generated
	 */
	EClass getEvent();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.discgolfmetrix.Event#getStartTime <em>Start Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start Time</em>'.
	 * @see tdt4250.discgolfmetrix.Event#getStartTime()
	 * @see #getEvent()
	 * @generated
	 */
	EAttribute getEvent_StartTime();

	/**
	 * Returns the meta object for the reference '{@link tdt4250.discgolfmetrix.Event#getCourse <em>Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Course</em>'.
	 * @see tdt4250.discgolfmetrix.Event#getCourse()
	 * @see #getEvent()
	 * @generated
	 */
	EReference getEvent_Course();

	/**
	 * Returns the meta object for the reference list '{@link tdt4250.discgolfmetrix.Event#getParticipant <em>Participant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Participant</em>'.
	 * @see tdt4250.discgolfmetrix.Event#getParticipant()
	 * @see #getEvent()
	 * @generated
	 */
	EReference getEvent_Participant();

	/**
	 * Returns the meta object for the reference '{@link tdt4250.discgolfmetrix.Event#getCoordinator <em>Coordinator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Coordinator</em>'.
	 * @see tdt4250.discgolfmetrix.Event#getCoordinator()
	 * @see #getEvent()
	 * @generated
	 */
	EReference getEvent_Coordinator();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.discgolfmetrix.Event#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see tdt4250.discgolfmetrix.Event#getName()
	 * @see #getEvent()
	 * @generated
	 */
	EAttribute getEvent_Name();

	/**
	 * Returns the meta object for class '{@link tdt4250.discgolfmetrix.Discgolfmetrix <em>Discgolfmetrix</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Discgolfmetrix</em>'.
	 * @see tdt4250.discgolfmetrix.Discgolfmetrix
	 * @generated
	 */
	EClass getDiscgolfmetrix();

	/**
	 * Returns the meta object for the containment reference '{@link tdt4250.discgolfmetrix.Discgolfmetrix#getUser <em>User</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>User</em>'.
	 * @see tdt4250.discgolfmetrix.Discgolfmetrix#getUser()
	 * @see #getDiscgolfmetrix()
	 * @generated
	 */
	EReference getDiscgolfmetrix_User();

	/**
	 * Returns the meta object for the containment reference '{@link tdt4250.discgolfmetrix.Discgolfmetrix#getCourse <em>Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Course</em>'.
	 * @see tdt4250.discgolfmetrix.Discgolfmetrix#getCourse()
	 * @see #getDiscgolfmetrix()
	 * @generated
	 */
	EReference getDiscgolfmetrix_Course();

	/**
	 * Returns the meta object for the containment reference '{@link tdt4250.discgolfmetrix.Discgolfmetrix#getClub <em>Club</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Club</em>'.
	 * @see tdt4250.discgolfmetrix.Discgolfmetrix#getClub()
	 * @see #getDiscgolfmetrix()
	 * @generated
	 */
	EReference getDiscgolfmetrix_Club();

	/**
	 * Returns the meta object for the containment reference '{@link tdt4250.discgolfmetrix.Discgolfmetrix#getEvent <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Event</em>'.
	 * @see tdt4250.discgolfmetrix.Discgolfmetrix#getEvent()
	 * @see #getDiscgolfmetrix()
	 * @generated
	 */
	EReference getDiscgolfmetrix_Event();

	/**
	 * Returns the meta object for enum '{@link tdt4250.discgolfmetrix.Par <em>Par</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Par</em>'.
	 * @see tdt4250.discgolfmetrix.Par
	 * @generated
	 */
	EEnum getPar();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	DiscgolfmetrixFactory getDiscgolfmetrixFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link tdt4250.discgolfmetrix.impl.UserImpl <em>User</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.discgolfmetrix.impl.UserImpl
		 * @see tdt4250.discgolfmetrix.impl.DiscgolfmetrixPackageImpl#getUser()
		 * @generated
		 */
		EClass USER = eINSTANCE.getUser();

		/**
		 * The meta object literal for the '<em><b>Firstname</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER__FIRSTNAME = eINSTANCE.getUser_Firstname();

		/**
		 * The meta object literal for the '<em><b>Lastname</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER__LASTNAME = eINSTANCE.getUser_Lastname();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER__NAME = eINSTANCE.getUser_Name();

		/**
		 * The meta object literal for the '<em><b>Nickname</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER__NICKNAME = eINSTANCE.getUser_Nickname();

		/**
		 * The meta object literal for the '<em><b>Email</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER__EMAIL = eINSTANCE.getUser_Email();

		/**
		 * The meta object literal for the '<em><b>PDG Anumber</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER__PDG_ANUMBER = eINSTANCE.getUser_PDGAnumber();

		/**
		 * The meta object literal for the '<em><b>User ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER__USER_ID = eINSTANCE.getUser_UserID();

		/**
		 * The meta object literal for the '<em><b>Rating</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER__RATING = eINSTANCE.getUser_Rating();

		/**
		 * The meta object literal for the '<em><b>Club</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference USER__CLUB = eINSTANCE.getUser_Club();

		/**
		 * The meta object literal for the '<em><b>Participant</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference USER__PARTICIPANT = eINSTANCE.getUser_Participant();

		/**
		 * The meta object literal for the '<em><b>Coordinator</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference USER__COORDINATOR = eINSTANCE.getUser_Coordinator();

		/**
		 * The meta object literal for the '{@link tdt4250.discgolfmetrix.impl.ClubImpl <em>Club</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.discgolfmetrix.impl.ClubImpl
		 * @see tdt4250.discgolfmetrix.impl.DiscgolfmetrixPackageImpl#getClub()
		 * @generated
		 */
		EClass CLUB = eINSTANCE.getClub();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLUB__NAME = eINSTANCE.getClub_Name();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLUB__DESCRIPTION = eINSTANCE.getClub_Description();

		/**
		 * The meta object literal for the '<em><b>Member</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLUB__MEMBER = eINSTANCE.getClub_Member();

		/**
		 * The meta object literal for the '{@link tdt4250.discgolfmetrix.impl.CourseImpl <em>Course</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.discgolfmetrix.impl.CourseImpl
		 * @see tdt4250.discgolfmetrix.impl.DiscgolfmetrixPackageImpl#getCourse()
		 * @generated
		 */
		EClass COURSE = eINSTANCE.getCourse();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__NAME = eINSTANCE.getCourse_Name();

		/**
		 * The meta object literal for the '<em><b>Baskets</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__BASKETS = eINSTANCE.getCourse_Baskets();

		/**
		 * The meta object literal for the '<em><b>Layouts</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE__LAYOUTS = eINSTANCE.getCourse_Layouts();

		/**
		 * The meta object literal for the '{@link tdt4250.discgolfmetrix.impl.CourseLayoutImpl <em>Course Layout</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.discgolfmetrix.impl.CourseLayoutImpl
		 * @see tdt4250.discgolfmetrix.impl.DiscgolfmetrixPackageImpl#getCourseLayout()
		 * @generated
		 */
		EClass COURSE_LAYOUT = eINSTANCE.getCourseLayout();

		/**
		 * The meta object literal for the '<em><b>Course</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE_LAYOUT__COURSE = eINSTANCE.getCourseLayout_Course();

		/**
		 * The meta object literal for the '<em><b>Hole</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE_LAYOUT__HOLE = eINSTANCE.getCourseLayout_Hole();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE_LAYOUT__NAME = eINSTANCE.getCourseLayout_Name();

		/**
		 * The meta object literal for the '{@link tdt4250.discgolfmetrix.impl.HoleImpl <em>Hole</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.discgolfmetrix.impl.HoleImpl
		 * @see tdt4250.discgolfmetrix.impl.DiscgolfmetrixPackageImpl#getHole()
		 * @generated
		 */
		EClass HOLE = eINSTANCE.getHole();

		/**
		 * The meta object literal for the '<em><b>Par</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HOLE__PAR = eINSTANCE.getHole_Par();

		/**
		 * The meta object literal for the '<em><b>Hole Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HOLE__HOLE_NUMBER = eINSTANCE.getHole_HoleNumber();

		/**
		 * The meta object literal for the '<em><b>Course Layout</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOLE__COURSE_LAYOUT = eINSTANCE.getHole_CourseLayout();

		/**
		 * The meta object literal for the '<em><b>Distance In Meters</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HOLE__DISTANCE_IN_METERS = eINSTANCE.getHole_DistanceInMeters();

		/**
		 * The meta object literal for the '<em><b>Distance In Feet</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HOLE__DISTANCE_IN_FEET = eINSTANCE.getHole_DistanceInFeet();

		/**
		 * The meta object literal for the '{@link tdt4250.discgolfmetrix.impl.EventImpl <em>Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.discgolfmetrix.impl.EventImpl
		 * @see tdt4250.discgolfmetrix.impl.DiscgolfmetrixPackageImpl#getEvent()
		 * @generated
		 */
		EClass EVENT = eINSTANCE.getEvent();

		/**
		 * The meta object literal for the '<em><b>Start Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVENT__START_TIME = eINSTANCE.getEvent_StartTime();

		/**
		 * The meta object literal for the '<em><b>Course</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT__COURSE = eINSTANCE.getEvent_Course();

		/**
		 * The meta object literal for the '<em><b>Participant</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT__PARTICIPANT = eINSTANCE.getEvent_Participant();

		/**
		 * The meta object literal for the '<em><b>Coordinator</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT__COORDINATOR = eINSTANCE.getEvent_Coordinator();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVENT__NAME = eINSTANCE.getEvent_Name();

		/**
		 * The meta object literal for the '{@link tdt4250.discgolfmetrix.impl.DiscgolfmetrixImpl <em>Discgolfmetrix</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.discgolfmetrix.impl.DiscgolfmetrixImpl
		 * @see tdt4250.discgolfmetrix.impl.DiscgolfmetrixPackageImpl#getDiscgolfmetrix()
		 * @generated
		 */
		EClass DISCGOLFMETRIX = eINSTANCE.getDiscgolfmetrix();

		/**
		 * The meta object literal for the '<em><b>User</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DISCGOLFMETRIX__USER = eINSTANCE.getDiscgolfmetrix_User();

		/**
		 * The meta object literal for the '<em><b>Course</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DISCGOLFMETRIX__COURSE = eINSTANCE.getDiscgolfmetrix_Course();

		/**
		 * The meta object literal for the '<em><b>Club</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DISCGOLFMETRIX__CLUB = eINSTANCE.getDiscgolfmetrix_Club();

		/**
		 * The meta object literal for the '<em><b>Event</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DISCGOLFMETRIX__EVENT = eINSTANCE.getDiscgolfmetrix_Event();

		/**
		 * The meta object literal for the '{@link tdt4250.discgolfmetrix.Par <em>Par</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.discgolfmetrix.Par
		 * @see tdt4250.discgolfmetrix.impl.DiscgolfmetrixPackageImpl#getPar()
		 * @generated
		 */
		EEnum PAR = eINSTANCE.getPar();

	}

} //DiscgolfmetrixPackage
