/**
 */
package tdt4250.discgolfmetrix;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Club</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.discgolfmetrix.Club#getName <em>Name</em>}</li>
 *   <li>{@link tdt4250.discgolfmetrix.Club#getDescription <em>Description</em>}</li>
 *   <li>{@link tdt4250.discgolfmetrix.Club#getMember <em>Member</em>}</li>
 * </ul>
 *
 * @see tdt4250.discgolfmetrix.DiscgolfmetrixPackage#getClub()
 * @model
 * @generated
 */
public interface Club extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see tdt4250.discgolfmetrix.DiscgolfmetrixPackage#getClub_Name()
	 * @model id="true" required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link tdt4250.discgolfmetrix.Club#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see tdt4250.discgolfmetrix.DiscgolfmetrixPackage#getClub_Description()
	 * @model required="true"
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link tdt4250.discgolfmetrix.Club#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Member</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250.discgolfmetrix.User}.
	 * It is bidirectional and its opposite is '{@link tdt4250.discgolfmetrix.User#getClub <em>Club</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Member</em>' containment reference list.
	 * @see tdt4250.discgolfmetrix.DiscgolfmetrixPackage#getClub_Member()
	 * @see tdt4250.discgolfmetrix.User#getClub
	 * @model opposite="club" containment="true"
	 * @generated
	 */
	EList<User> getMember();

} // Club
