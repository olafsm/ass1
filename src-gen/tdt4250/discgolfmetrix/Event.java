/**
 */
package tdt4250.discgolfmetrix;

import java.util.Date;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.discgolfmetrix.Event#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link tdt4250.discgolfmetrix.Event#getCourse <em>Course</em>}</li>
 *   <li>{@link tdt4250.discgolfmetrix.Event#getParticipant <em>Participant</em>}</li>
 *   <li>{@link tdt4250.discgolfmetrix.Event#getCoordinator <em>Coordinator</em>}</li>
 *   <li>{@link tdt4250.discgolfmetrix.Event#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see tdt4250.discgolfmetrix.DiscgolfmetrixPackage#getEvent()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='participantNotCoordinator'"
 * @generated
 */
public interface Event extends EObject {
	/**
	 * Returns the value of the '<em><b>Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start Time</em>' attribute.
	 * @see #setStartTime(Date)
	 * @see tdt4250.discgolfmetrix.DiscgolfmetrixPackage#getEvent_StartTime()
	 * @model
	 * @generated
	 */
	Date getStartTime();

	/**
	 * Sets the value of the '{@link tdt4250.discgolfmetrix.Event#getStartTime <em>Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start Time</em>' attribute.
	 * @see #getStartTime()
	 * @generated
	 */
	void setStartTime(Date value);

	/**
	 * Returns the value of the '<em><b>Course</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Course</em>' reference.
	 * @see #setCourse(CourseLayout)
	 * @see tdt4250.discgolfmetrix.DiscgolfmetrixPackage#getEvent_Course()
	 * @model
	 * @generated
	 */
	CourseLayout getCourse();

	/**
	 * Sets the value of the '{@link tdt4250.discgolfmetrix.Event#getCourse <em>Course</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Course</em>' reference.
	 * @see #getCourse()
	 * @generated
	 */
	void setCourse(CourseLayout value);

	/**
	 * Returns the value of the '<em><b>Participant</b></em>' reference list.
	 * The list contents are of type {@link tdt4250.discgolfmetrix.User}.
	 * It is bidirectional and its opposite is '{@link tdt4250.discgolfmetrix.User#getParticipant <em>Participant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Participant</em>' reference list.
	 * @see tdt4250.discgolfmetrix.DiscgolfmetrixPackage#getEvent_Participant()
	 * @see tdt4250.discgolfmetrix.User#getParticipant
	 * @model opposite="participant"
	 * @generated
	 */
	EList<User> getParticipant();

	/**
	 * Returns the value of the '<em><b>Coordinator</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link tdt4250.discgolfmetrix.User#getCoordinator <em>Coordinator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Coordinator</em>' reference.
	 * @see #setCoordinator(User)
	 * @see tdt4250.discgolfmetrix.DiscgolfmetrixPackage#getEvent_Coordinator()
	 * @see tdt4250.discgolfmetrix.User#getCoordinator
	 * @model opposite="coordinator" required="true"
	 * @generated
	 */
	User getCoordinator();

	/**
	 * Sets the value of the '{@link tdt4250.discgolfmetrix.Event#getCoordinator <em>Coordinator</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Coordinator</em>' reference.
	 * @see #getCoordinator()
	 * @generated
	 */
	void setCoordinator(User value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see tdt4250.discgolfmetrix.DiscgolfmetrixPackage#getEvent_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link tdt4250.discgolfmetrix.Event#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

} // Event
