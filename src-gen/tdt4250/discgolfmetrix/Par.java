/**
 */
package tdt4250.discgolfmetrix;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Par</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see tdt4250.discgolfmetrix.DiscgolfmetrixPackage#getPar()
 * @model
 * @generated
 */
public enum Par implements Enumerator {
	/**
	 * The '<em><b>Par2</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PAR2_VALUE
	 * @generated
	 * @ordered
	 */
	PAR2(2, "Par2", "Par2"),

	/**
	 * The '<em><b>Par3</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PAR3_VALUE
	 * @generated
	 * @ordered
	 */
	PAR3(3, "Par3", "Par3"),

	/**
	 * The '<em><b>Par4</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PAR4_VALUE
	 * @generated
	 * @ordered
	 */
	PAR4(4, "Par4", "Par4"),

	/**
	 * The '<em><b>Par5</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PAR5_VALUE
	 * @generated
	 * @ordered
	 */
	PAR5(5, "Par5", "Par5"),

	/**
	 * The '<em><b>Par6</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PAR6_VALUE
	 * @generated
	 * @ordered
	 */
	PAR6(6, "Par6", "Par6"),

	/**
	 * The '<em><b>Par7</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PAR7_VALUE
	 * @generated
	 * @ordered
	 */
	PAR7(7, "Par7", "Par7");

	/**
	 * The '<em><b>Par2</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PAR2
	 * @model name="Par2"
	 * @generated
	 * @ordered
	 */
	public static final int PAR2_VALUE = 2;

	/**
	 * The '<em><b>Par3</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PAR3
	 * @model name="Par3"
	 * @generated
	 * @ordered
	 */
	public static final int PAR3_VALUE = 3;

	/**
	 * The '<em><b>Par4</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PAR4
	 * @model name="Par4"
	 * @generated
	 * @ordered
	 */
	public static final int PAR4_VALUE = 4;

	/**
	 * The '<em><b>Par5</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PAR5
	 * @model name="Par5"
	 * @generated
	 * @ordered
	 */
	public static final int PAR5_VALUE = 5;

	/**
	 * The '<em><b>Par6</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PAR6
	 * @model name="Par6"
	 * @generated
	 * @ordered
	 */
	public static final int PAR6_VALUE = 6;

	/**
	 * The '<em><b>Par7</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PAR7
	 * @model name="Par7"
	 * @generated
	 * @ordered
	 */
	public static final int PAR7_VALUE = 7;

	/**
	 * An array of all the '<em><b>Par</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final Par[] VALUES_ARRAY = new Par[] { PAR2, PAR3, PAR4, PAR5, PAR6, PAR7, };

	/**
	 * A public read-only list of all the '<em><b>Par</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<Par> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Par</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static Par get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			Par result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Par</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static Par getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			Par result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Par</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static Par get(int value) {
		switch (value) {
		case PAR2_VALUE:
			return PAR2;
		case PAR3_VALUE:
			return PAR3;
		case PAR4_VALUE:
			return PAR4;
		case PAR5_VALUE:
			return PAR5;
		case PAR6_VALUE:
			return PAR6;
		case PAR7_VALUE:
			return PAR7;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private Par(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
		return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}

} //Par
