/**
 */
package tdt4250.discgolfmetrix.impl;

import java.util.Collection;
import java.util.Date;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import tdt4250.discgolfmetrix.CourseLayout;
import tdt4250.discgolfmetrix.DiscgolfmetrixPackage;
import tdt4250.discgolfmetrix.Event;
import tdt4250.discgolfmetrix.User;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Event</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.discgolfmetrix.impl.EventImpl#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link tdt4250.discgolfmetrix.impl.EventImpl#getCourse <em>Course</em>}</li>
 *   <li>{@link tdt4250.discgolfmetrix.impl.EventImpl#getParticipant <em>Participant</em>}</li>
 *   <li>{@link tdt4250.discgolfmetrix.impl.EventImpl#getCoordinator <em>Coordinator</em>}</li>
 *   <li>{@link tdt4250.discgolfmetrix.impl.EventImpl#getName <em>Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EventImpl extends MinimalEObjectImpl.Container implements Event {
	/**
	 * The default value of the '{@link #getStartTime() <em>Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartTime()
	 * @generated
	 * @ordered
	 */
	protected static final Date START_TIME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getStartTime() <em>Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartTime()
	 * @generated
	 * @ordered
	 */
	protected Date startTime = START_TIME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCourse() <em>Course</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourse()
	 * @generated
	 * @ordered
	 */
	protected CourseLayout course;

	/**
	 * The cached value of the '{@link #getParticipant() <em>Participant</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParticipant()
	 * @generated
	 * @ordered
	 */
	protected EList<User> participant;

	/**
	 * The cached value of the '{@link #getCoordinator() <em>Coordinator</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCoordinator()
	 * @generated
	 * @ordered
	 */
	protected User coordinator;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DiscgolfmetrixPackage.Literals.EVENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getStartTime() {
		return startTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStartTime(Date newStartTime) {
		Date oldStartTime = startTime;
		startTime = newStartTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DiscgolfmetrixPackage.EVENT__START_TIME, oldStartTime,
					startTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CourseLayout getCourse() {
		if (course != null && course.eIsProxy()) {
			InternalEObject oldCourse = (InternalEObject) course;
			course = (CourseLayout) eResolveProxy(oldCourse);
			if (course != oldCourse) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DiscgolfmetrixPackage.EVENT__COURSE,
							oldCourse, course));
			}
		}
		return course;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CourseLayout basicGetCourse() {
		return course;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCourse(CourseLayout newCourse) {
		CourseLayout oldCourse = course;
		course = newCourse;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DiscgolfmetrixPackage.EVENT__COURSE, oldCourse,
					course));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<User> getParticipant() {
		if (participant == null) {
			participant = new EObjectWithInverseResolvingEList.ManyInverse<User>(User.class, this,
					DiscgolfmetrixPackage.EVENT__PARTICIPANT, DiscgolfmetrixPackage.USER__PARTICIPANT);
		}
		return participant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public User getCoordinator() {
		if (coordinator != null && coordinator.eIsProxy()) {
			InternalEObject oldCoordinator = (InternalEObject) coordinator;
			coordinator = (User) eResolveProxy(oldCoordinator);
			if (coordinator != oldCoordinator) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DiscgolfmetrixPackage.EVENT__COORDINATOR,
							oldCoordinator, coordinator));
			}
		}
		return coordinator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public User basicGetCoordinator() {
		return coordinator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCoordinator(User newCoordinator, NotificationChain msgs) {
		User oldCoordinator = coordinator;
		coordinator = newCoordinator;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					DiscgolfmetrixPackage.EVENT__COORDINATOR, oldCoordinator, newCoordinator);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCoordinator(User newCoordinator) {
		if (newCoordinator != coordinator) {
			NotificationChain msgs = null;
			if (coordinator != null)
				msgs = ((InternalEObject) coordinator).eInverseRemove(this, DiscgolfmetrixPackage.USER__COORDINATOR,
						User.class, msgs);
			if (newCoordinator != null)
				msgs = ((InternalEObject) newCoordinator).eInverseAdd(this, DiscgolfmetrixPackage.USER__COORDINATOR,
						User.class, msgs);
			msgs = basicSetCoordinator(newCoordinator, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DiscgolfmetrixPackage.EVENT__COORDINATOR,
					newCoordinator, newCoordinator));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DiscgolfmetrixPackage.EVENT__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case DiscgolfmetrixPackage.EVENT__PARTICIPANT:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getParticipant()).basicAdd(otherEnd, msgs);
		case DiscgolfmetrixPackage.EVENT__COORDINATOR:
			if (coordinator != null)
				msgs = ((InternalEObject) coordinator).eInverseRemove(this, DiscgolfmetrixPackage.USER__COORDINATOR,
						User.class, msgs);
			return basicSetCoordinator((User) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case DiscgolfmetrixPackage.EVENT__PARTICIPANT:
			return ((InternalEList<?>) getParticipant()).basicRemove(otherEnd, msgs);
		case DiscgolfmetrixPackage.EVENT__COORDINATOR:
			return basicSetCoordinator(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case DiscgolfmetrixPackage.EVENT__START_TIME:
			return getStartTime();
		case DiscgolfmetrixPackage.EVENT__COURSE:
			if (resolve)
				return getCourse();
			return basicGetCourse();
		case DiscgolfmetrixPackage.EVENT__PARTICIPANT:
			return getParticipant();
		case DiscgolfmetrixPackage.EVENT__COORDINATOR:
			if (resolve)
				return getCoordinator();
			return basicGetCoordinator();
		case DiscgolfmetrixPackage.EVENT__NAME:
			return getName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case DiscgolfmetrixPackage.EVENT__START_TIME:
			setStartTime((Date) newValue);
			return;
		case DiscgolfmetrixPackage.EVENT__COURSE:
			setCourse((CourseLayout) newValue);
			return;
		case DiscgolfmetrixPackage.EVENT__PARTICIPANT:
			getParticipant().clear();
			getParticipant().addAll((Collection<? extends User>) newValue);
			return;
		case DiscgolfmetrixPackage.EVENT__COORDINATOR:
			setCoordinator((User) newValue);
			return;
		case DiscgolfmetrixPackage.EVENT__NAME:
			setName((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case DiscgolfmetrixPackage.EVENT__START_TIME:
			setStartTime(START_TIME_EDEFAULT);
			return;
		case DiscgolfmetrixPackage.EVENT__COURSE:
			setCourse((CourseLayout) null);
			return;
		case DiscgolfmetrixPackage.EVENT__PARTICIPANT:
			getParticipant().clear();
			return;
		case DiscgolfmetrixPackage.EVENT__COORDINATOR:
			setCoordinator((User) null);
			return;
		case DiscgolfmetrixPackage.EVENT__NAME:
			setName(NAME_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case DiscgolfmetrixPackage.EVENT__START_TIME:
			return START_TIME_EDEFAULT == null ? startTime != null : !START_TIME_EDEFAULT.equals(startTime);
		case DiscgolfmetrixPackage.EVENT__COURSE:
			return course != null;
		case DiscgolfmetrixPackage.EVENT__PARTICIPANT:
			return participant != null && !participant.isEmpty();
		case DiscgolfmetrixPackage.EVENT__COORDINATOR:
			return coordinator != null;
		case DiscgolfmetrixPackage.EVENT__NAME:
			return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (startTime: ");
		result.append(startTime);
		result.append(", name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //EventImpl
