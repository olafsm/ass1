/**
 */
package tdt4250.discgolfmetrix.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import tdt4250.discgolfmetrix.Club;
import tdt4250.discgolfmetrix.Course;
import tdt4250.discgolfmetrix.Discgolfmetrix;
import tdt4250.discgolfmetrix.DiscgolfmetrixPackage;
import tdt4250.discgolfmetrix.Event;
import tdt4250.discgolfmetrix.User;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Discgolfmetrix</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.discgolfmetrix.impl.DiscgolfmetrixImpl#getUser <em>User</em>}</li>
 *   <li>{@link tdt4250.discgolfmetrix.impl.DiscgolfmetrixImpl#getCourse <em>Course</em>}</li>
 *   <li>{@link tdt4250.discgolfmetrix.impl.DiscgolfmetrixImpl#getClub <em>Club</em>}</li>
 *   <li>{@link tdt4250.discgolfmetrix.impl.DiscgolfmetrixImpl#getEvent <em>Event</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DiscgolfmetrixImpl extends MinimalEObjectImpl.Container implements Discgolfmetrix {
	/**
	 * The cached value of the '{@link #getUser() <em>User</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUser()
	 * @generated
	 * @ordered
	 */
	protected User user;

	/**
	 * The cached value of the '{@link #getCourse() <em>Course</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourse()
	 * @generated
	 * @ordered
	 */
	protected Course course;

	/**
	 * The cached value of the '{@link #getClub() <em>Club</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClub()
	 * @generated
	 * @ordered
	 */
	protected Club club;

	/**
	 * The cached value of the '{@link #getEvent() <em>Event</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEvent()
	 * @generated
	 * @ordered
	 */
	protected Event event;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DiscgolfmetrixImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DiscgolfmetrixPackage.Literals.DISCGOLFMETRIX;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public User getUser() {
		return user;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUser(User newUser, NotificationChain msgs) {
		User oldUser = user;
		user = newUser;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					DiscgolfmetrixPackage.DISCGOLFMETRIX__USER, oldUser, newUser);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUser(User newUser) {
		if (newUser != user) {
			NotificationChain msgs = null;
			if (user != null)
				msgs = ((InternalEObject) user).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - DiscgolfmetrixPackage.DISCGOLFMETRIX__USER, null, msgs);
			if (newUser != null)
				msgs = ((InternalEObject) newUser).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - DiscgolfmetrixPackage.DISCGOLFMETRIX__USER, null, msgs);
			msgs = basicSetUser(newUser, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DiscgolfmetrixPackage.DISCGOLFMETRIX__USER, newUser,
					newUser));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Course getCourse() {
		return course;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCourse(Course newCourse, NotificationChain msgs) {
		Course oldCourse = course;
		course = newCourse;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					DiscgolfmetrixPackage.DISCGOLFMETRIX__COURSE, oldCourse, newCourse);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCourse(Course newCourse) {
		if (newCourse != course) {
			NotificationChain msgs = null;
			if (course != null)
				msgs = ((InternalEObject) course).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - DiscgolfmetrixPackage.DISCGOLFMETRIX__COURSE, null, msgs);
			if (newCourse != null)
				msgs = ((InternalEObject) newCourse).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - DiscgolfmetrixPackage.DISCGOLFMETRIX__COURSE, null, msgs);
			msgs = basicSetCourse(newCourse, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DiscgolfmetrixPackage.DISCGOLFMETRIX__COURSE,
					newCourse, newCourse));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Club getClub() {
		return club;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetClub(Club newClub, NotificationChain msgs) {
		Club oldClub = club;
		club = newClub;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					DiscgolfmetrixPackage.DISCGOLFMETRIX__CLUB, oldClub, newClub);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setClub(Club newClub) {
		if (newClub != club) {
			NotificationChain msgs = null;
			if (club != null)
				msgs = ((InternalEObject) club).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - DiscgolfmetrixPackage.DISCGOLFMETRIX__CLUB, null, msgs);
			if (newClub != null)
				msgs = ((InternalEObject) newClub).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - DiscgolfmetrixPackage.DISCGOLFMETRIX__CLUB, null, msgs);
			msgs = basicSetClub(newClub, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DiscgolfmetrixPackage.DISCGOLFMETRIX__CLUB, newClub,
					newClub));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Event getEvent() {
		return event;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEvent(Event newEvent, NotificationChain msgs) {
		Event oldEvent = event;
		event = newEvent;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					DiscgolfmetrixPackage.DISCGOLFMETRIX__EVENT, oldEvent, newEvent);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEvent(Event newEvent) {
		if (newEvent != event) {
			NotificationChain msgs = null;
			if (event != null)
				msgs = ((InternalEObject) event).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - DiscgolfmetrixPackage.DISCGOLFMETRIX__EVENT, null, msgs);
			if (newEvent != null)
				msgs = ((InternalEObject) newEvent).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - DiscgolfmetrixPackage.DISCGOLFMETRIX__EVENT, null, msgs);
			msgs = basicSetEvent(newEvent, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DiscgolfmetrixPackage.DISCGOLFMETRIX__EVENT, newEvent,
					newEvent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case DiscgolfmetrixPackage.DISCGOLFMETRIX__USER:
			return basicSetUser(null, msgs);
		case DiscgolfmetrixPackage.DISCGOLFMETRIX__COURSE:
			return basicSetCourse(null, msgs);
		case DiscgolfmetrixPackage.DISCGOLFMETRIX__CLUB:
			return basicSetClub(null, msgs);
		case DiscgolfmetrixPackage.DISCGOLFMETRIX__EVENT:
			return basicSetEvent(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case DiscgolfmetrixPackage.DISCGOLFMETRIX__USER:
			return getUser();
		case DiscgolfmetrixPackage.DISCGOLFMETRIX__COURSE:
			return getCourse();
		case DiscgolfmetrixPackage.DISCGOLFMETRIX__CLUB:
			return getClub();
		case DiscgolfmetrixPackage.DISCGOLFMETRIX__EVENT:
			return getEvent();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case DiscgolfmetrixPackage.DISCGOLFMETRIX__USER:
			setUser((User) newValue);
			return;
		case DiscgolfmetrixPackage.DISCGOLFMETRIX__COURSE:
			setCourse((Course) newValue);
			return;
		case DiscgolfmetrixPackage.DISCGOLFMETRIX__CLUB:
			setClub((Club) newValue);
			return;
		case DiscgolfmetrixPackage.DISCGOLFMETRIX__EVENT:
			setEvent((Event) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case DiscgolfmetrixPackage.DISCGOLFMETRIX__USER:
			setUser((User) null);
			return;
		case DiscgolfmetrixPackage.DISCGOLFMETRIX__COURSE:
			setCourse((Course) null);
			return;
		case DiscgolfmetrixPackage.DISCGOLFMETRIX__CLUB:
			setClub((Club) null);
			return;
		case DiscgolfmetrixPackage.DISCGOLFMETRIX__EVENT:
			setEvent((Event) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case DiscgolfmetrixPackage.DISCGOLFMETRIX__USER:
			return user != null;
		case DiscgolfmetrixPackage.DISCGOLFMETRIX__COURSE:
			return course != null;
		case DiscgolfmetrixPackage.DISCGOLFMETRIX__CLUB:
			return club != null;
		case DiscgolfmetrixPackage.DISCGOLFMETRIX__EVENT:
			return event != null;
		}
		return super.eIsSet(featureID);
	}

} //DiscgolfmetrixImpl
