/**
 */
package tdt4250.discgolfmetrix.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import tdt4250.discgolfmetrix.Club;
import tdt4250.discgolfmetrix.DiscgolfmetrixPackage;
import tdt4250.discgolfmetrix.Event;
import tdt4250.discgolfmetrix.User;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>User</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.discgolfmetrix.impl.UserImpl#getFirstname <em>Firstname</em>}</li>
 *   <li>{@link tdt4250.discgolfmetrix.impl.UserImpl#getLastname <em>Lastname</em>}</li>
 *   <li>{@link tdt4250.discgolfmetrix.impl.UserImpl#getName <em>Name</em>}</li>
 *   <li>{@link tdt4250.discgolfmetrix.impl.UserImpl#getNickname <em>Nickname</em>}</li>
 *   <li>{@link tdt4250.discgolfmetrix.impl.UserImpl#getEmail <em>Email</em>}</li>
 *   <li>{@link tdt4250.discgolfmetrix.impl.UserImpl#getPDGAnumber <em>PDG Anumber</em>}</li>
 *   <li>{@link tdt4250.discgolfmetrix.impl.UserImpl#getUserID <em>User ID</em>}</li>
 *   <li>{@link tdt4250.discgolfmetrix.impl.UserImpl#getRating <em>Rating</em>}</li>
 *   <li>{@link tdt4250.discgolfmetrix.impl.UserImpl#getClub <em>Club</em>}</li>
 *   <li>{@link tdt4250.discgolfmetrix.impl.UserImpl#getParticipant <em>Participant</em>}</li>
 *   <li>{@link tdt4250.discgolfmetrix.impl.UserImpl#getCoordinator <em>Coordinator</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UserImpl extends MinimalEObjectImpl.Container implements User {
	/**
	 * The default value of the '{@link #getFirstname() <em>Firstname</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFirstname()
	 * @generated
	 * @ordered
	 */
	protected static final String FIRSTNAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFirstname() <em>Firstname</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFirstname()
	 * @generated
	 * @ordered
	 */
	protected String firstname = FIRSTNAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getLastname() <em>Lastname</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastname()
	 * @generated
	 * @ordered
	 */
	protected static final String LASTNAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLastname() <em>Lastname</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastname()
	 * @generated
	 * @ordered
	 */
	protected String lastname = LASTNAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * This is true if the Name attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean nameESet;

	/**
	 * The default value of the '{@link #getNickname() <em>Nickname</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNickname()
	 * @generated
	 * @ordered
	 */
	protected static final String NICKNAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getNickname() <em>Nickname</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNickname()
	 * @generated
	 * @ordered
	 */
	protected String nickname = NICKNAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getEmail() <em>Email</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEmail()
	 * @generated
	 * @ordered
	 */
	protected static final String EMAIL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getEmail() <em>Email</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEmail()
	 * @generated
	 * @ordered
	 */
	protected String email = EMAIL_EDEFAULT;

	/**
	 * The default value of the '{@link #getPDGAnumber() <em>PDG Anumber</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPDGAnumber()
	 * @generated
	 * @ordered
	 */
	protected static final Integer PDG_ANUMBER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPDGAnumber() <em>PDG Anumber</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPDGAnumber()
	 * @generated
	 * @ordered
	 */
	protected Integer pdgAnumber = PDG_ANUMBER_EDEFAULT;

	/**
	 * The default value of the '{@link #getUserID() <em>User ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUserID()
	 * @generated
	 * @ordered
	 */
	protected static final Integer USER_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUserID() <em>User ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUserID()
	 * @generated
	 * @ordered
	 */
	protected Integer userID = USER_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getRating() <em>Rating</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRating()
	 * @generated
	 * @ordered
	 */
	protected static final Integer RATING_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRating() <em>Rating</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRating()
	 * @generated
	 * @ordered
	 */
	protected Integer rating = RATING_EDEFAULT;

	/**
	 * The cached value of the '{@link #getParticipant() <em>Participant</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParticipant()
	 * @generated
	 * @ordered
	 */
	protected EList<Event> participant;

	/**
	 * The cached value of the '{@link #getCoordinator() <em>Coordinator</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCoordinator()
	 * @generated
	 * @ordered
	 */
	protected EList<Event> coordinator;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UserImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DiscgolfmetrixPackage.Literals.USER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getFirstname() {
		return firstname;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFirstname(String newFirstname) {
		String oldFirstname = firstname;
		firstname = newFirstname;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DiscgolfmetrixPackage.USER__FIRSTNAME, oldFirstname,
					firstname));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLastname() {
		return lastname;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLastname(String newLastname) {
		String oldLastname = lastname;
		lastname = newLastname;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DiscgolfmetrixPackage.USER__LASTNAME, oldLastname,
					lastname));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String getName() {
		String fn = getFirstname();
		String ln = getLastname();
		if(fn != null && ln != null ) {
			return fn + " " + ln;
		}
		if(fn != null) {
			return fn;
		}
		if(ln != null) {
			return ln;
		}
		return " ";
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetName() {
		return nameESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getNickname() {
		return nickname;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNickname(String newNickname) {
		String oldNickname = nickname;
		nickname = newNickname;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DiscgolfmetrixPackage.USER__NICKNAME, oldNickname,
					nickname));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEmail(String newEmail) {
		String oldEmail = email;
		email = newEmail;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DiscgolfmetrixPackage.USER__EMAIL, oldEmail, email));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integer getPDGAnumber() {
		return pdgAnumber;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPDGAnumber(Integer newPDGAnumber) {
		Integer oldPDGAnumber = pdgAnumber;
		pdgAnumber = newPDGAnumber;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DiscgolfmetrixPackage.USER__PDG_ANUMBER,
					oldPDGAnumber, pdgAnumber));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integer getUserID() {
		return userID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUserID(Integer newUserID) {
		Integer oldUserID = userID;
		userID = newUserID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DiscgolfmetrixPackage.USER__USER_ID, oldUserID,
					userID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integer getRating() {
		return rating;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRating(Integer newRating) {
		Integer oldRating = rating;
		rating = newRating;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DiscgolfmetrixPackage.USER__RATING, oldRating,
					rating));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Club getClub() {
		if (eContainerFeatureID() != DiscgolfmetrixPackage.USER__CLUB)
			return null;
		return (Club) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetClub(Club newClub, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newClub, DiscgolfmetrixPackage.USER__CLUB, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setClub(Club newClub) {
		if (newClub != eInternalContainer()
				|| (eContainerFeatureID() != DiscgolfmetrixPackage.USER__CLUB && newClub != null)) {
			if (EcoreUtil.isAncestor(this, newClub))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newClub != null)
				msgs = ((InternalEObject) newClub).eInverseAdd(this, DiscgolfmetrixPackage.CLUB__MEMBER, Club.class,
						msgs);
			msgs = basicSetClub(newClub, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DiscgolfmetrixPackage.USER__CLUB, newClub, newClub));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Event> getParticipant() {
		if (participant == null) {
			participant = new EObjectWithInverseResolvingEList.ManyInverse<Event>(Event.class, this,
					DiscgolfmetrixPackage.USER__PARTICIPANT, DiscgolfmetrixPackage.EVENT__PARTICIPANT);
		}
		return participant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Event> getCoordinator() {
		if (coordinator == null) {
			coordinator = new EObjectWithInverseResolvingEList<Event>(Event.class, this,
					DiscgolfmetrixPackage.USER__COORDINATOR, DiscgolfmetrixPackage.EVENT__COORDINATOR);
		}
		return coordinator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case DiscgolfmetrixPackage.USER__CLUB:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetClub((Club) otherEnd, msgs);
		case DiscgolfmetrixPackage.USER__PARTICIPANT:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getParticipant()).basicAdd(otherEnd, msgs);
		case DiscgolfmetrixPackage.USER__COORDINATOR:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getCoordinator()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case DiscgolfmetrixPackage.USER__CLUB:
			return basicSetClub(null, msgs);
		case DiscgolfmetrixPackage.USER__PARTICIPANT:
			return ((InternalEList<?>) getParticipant()).basicRemove(otherEnd, msgs);
		case DiscgolfmetrixPackage.USER__COORDINATOR:
			return ((InternalEList<?>) getCoordinator()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case DiscgolfmetrixPackage.USER__CLUB:
			return eInternalContainer().eInverseRemove(this, DiscgolfmetrixPackage.CLUB__MEMBER, Club.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case DiscgolfmetrixPackage.USER__FIRSTNAME:
			return getFirstname();
		case DiscgolfmetrixPackage.USER__LASTNAME:
			return getLastname();
		case DiscgolfmetrixPackage.USER__NAME:
			return getName();
		case DiscgolfmetrixPackage.USER__NICKNAME:
			return getNickname();
		case DiscgolfmetrixPackage.USER__EMAIL:
			return getEmail();
		case DiscgolfmetrixPackage.USER__PDG_ANUMBER:
			return getPDGAnumber();
		case DiscgolfmetrixPackage.USER__USER_ID:
			return getUserID();
		case DiscgolfmetrixPackage.USER__RATING:
			return getRating();
		case DiscgolfmetrixPackage.USER__CLUB:
			return getClub();
		case DiscgolfmetrixPackage.USER__PARTICIPANT:
			return getParticipant();
		case DiscgolfmetrixPackage.USER__COORDINATOR:
			return getCoordinator();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case DiscgolfmetrixPackage.USER__FIRSTNAME:
			setFirstname((String) newValue);
			return;
		case DiscgolfmetrixPackage.USER__LASTNAME:
			setLastname((String) newValue);
			return;
		case DiscgolfmetrixPackage.USER__NICKNAME:
			setNickname((String) newValue);
			return;
		case DiscgolfmetrixPackage.USER__EMAIL:
			setEmail((String) newValue);
			return;
		case DiscgolfmetrixPackage.USER__PDG_ANUMBER:
			setPDGAnumber((Integer) newValue);
			return;
		case DiscgolfmetrixPackage.USER__USER_ID:
			setUserID((Integer) newValue);
			return;
		case DiscgolfmetrixPackage.USER__RATING:
			setRating((Integer) newValue);
			return;
		case DiscgolfmetrixPackage.USER__CLUB:
			setClub((Club) newValue);
			return;
		case DiscgolfmetrixPackage.USER__PARTICIPANT:
			getParticipant().clear();
			getParticipant().addAll((Collection<? extends Event>) newValue);
			return;
		case DiscgolfmetrixPackage.USER__COORDINATOR:
			getCoordinator().clear();
			getCoordinator().addAll((Collection<? extends Event>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case DiscgolfmetrixPackage.USER__FIRSTNAME:
			setFirstname(FIRSTNAME_EDEFAULT);
			return;
		case DiscgolfmetrixPackage.USER__LASTNAME:
			setLastname(LASTNAME_EDEFAULT);
			return;
		case DiscgolfmetrixPackage.USER__NICKNAME:
			setNickname(NICKNAME_EDEFAULT);
			return;
		case DiscgolfmetrixPackage.USER__EMAIL:
			setEmail(EMAIL_EDEFAULT);
			return;
		case DiscgolfmetrixPackage.USER__PDG_ANUMBER:
			setPDGAnumber(PDG_ANUMBER_EDEFAULT);
			return;
		case DiscgolfmetrixPackage.USER__USER_ID:
			setUserID(USER_ID_EDEFAULT);
			return;
		case DiscgolfmetrixPackage.USER__RATING:
			setRating(RATING_EDEFAULT);
			return;
		case DiscgolfmetrixPackage.USER__CLUB:
			setClub((Club) null);
			return;
		case DiscgolfmetrixPackage.USER__PARTICIPANT:
			getParticipant().clear();
			return;
		case DiscgolfmetrixPackage.USER__COORDINATOR:
			getCoordinator().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case DiscgolfmetrixPackage.USER__FIRSTNAME:
			return FIRSTNAME_EDEFAULT == null ? firstname != null : !FIRSTNAME_EDEFAULT.equals(firstname);
		case DiscgolfmetrixPackage.USER__LASTNAME:
			return LASTNAME_EDEFAULT == null ? lastname != null : !LASTNAME_EDEFAULT.equals(lastname);
		case DiscgolfmetrixPackage.USER__NAME:
			return isSetName();
		case DiscgolfmetrixPackage.USER__NICKNAME:
			return NICKNAME_EDEFAULT == null ? nickname != null : !NICKNAME_EDEFAULT.equals(nickname);
		case DiscgolfmetrixPackage.USER__EMAIL:
			return EMAIL_EDEFAULT == null ? email != null : !EMAIL_EDEFAULT.equals(email);
		case DiscgolfmetrixPackage.USER__PDG_ANUMBER:
			return PDG_ANUMBER_EDEFAULT == null ? pdgAnumber != null : !PDG_ANUMBER_EDEFAULT.equals(pdgAnumber);
		case DiscgolfmetrixPackage.USER__USER_ID:
			return USER_ID_EDEFAULT == null ? userID != null : !USER_ID_EDEFAULT.equals(userID);
		case DiscgolfmetrixPackage.USER__RATING:
			return RATING_EDEFAULT == null ? rating != null : !RATING_EDEFAULT.equals(rating);
		case DiscgolfmetrixPackage.USER__CLUB:
			return getClub() != null;
		case DiscgolfmetrixPackage.USER__PARTICIPANT:
			return participant != null && !participant.isEmpty();
		case DiscgolfmetrixPackage.USER__COORDINATOR:
			return coordinator != null && !coordinator.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (firstname: ");
		result.append(firstname);
		result.append(", lastname: ");
		result.append(lastname);
		result.append(", name: ");
		if (nameESet)
			result.append(name);
		else
			result.append("<unset>");
		result.append(", nickname: ");
		result.append(nickname);
		result.append(", email: ");
		result.append(email);
		result.append(", PDGAnumber: ");
		result.append(pdgAnumber);
		result.append(", userID: ");
		result.append(userID);
		result.append(", rating: ");
		result.append(rating);
		result.append(')');
		return result.toString();
	}

} //UserImpl
