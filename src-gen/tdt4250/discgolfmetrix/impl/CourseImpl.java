/**
 */
package tdt4250.discgolfmetrix.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

import tdt4250.discgolfmetrix.Course;
import tdt4250.discgolfmetrix.CourseLayout;
import tdt4250.discgolfmetrix.DiscgolfmetrixPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Course</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.discgolfmetrix.impl.CourseImpl#getName <em>Name</em>}</li>
 *   <li>{@link tdt4250.discgolfmetrix.impl.CourseImpl#getBaskets <em>Baskets</em>}</li>
 *   <li>{@link tdt4250.discgolfmetrix.impl.CourseImpl#getLayouts <em>Layouts</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CourseImpl extends MinimalEObjectImpl.Container implements Course {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getBaskets() <em>Baskets</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaskets()
	 * @generated
	 * @ordered
	 */
	protected static final Integer BASKETS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getBaskets() <em>Baskets</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaskets()
	 * @generated
	 * @ordered
	 */
	protected Integer baskets = BASKETS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLayouts() <em>Layouts</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLayouts()
	 * @generated
	 * @ordered
	 */
	protected EList<CourseLayout> layouts;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CourseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DiscgolfmetrixPackage.Literals.COURSE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DiscgolfmetrixPackage.COURSE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integer getBaskets() {
		return baskets;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBaskets(Integer newBaskets) {
		Integer oldBaskets = baskets;
		baskets = newBaskets;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DiscgolfmetrixPackage.COURSE__BASKETS, oldBaskets,
					baskets));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CourseLayout> getLayouts() {
		if (layouts == null) {
			layouts = new EObjectContainmentWithInverseEList<CourseLayout>(CourseLayout.class, this,
					DiscgolfmetrixPackage.COURSE__LAYOUTS, DiscgolfmetrixPackage.COURSE_LAYOUT__COURSE);
		}
		return layouts;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case DiscgolfmetrixPackage.COURSE__LAYOUTS:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getLayouts()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case DiscgolfmetrixPackage.COURSE__LAYOUTS:
			return ((InternalEList<?>) getLayouts()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case DiscgolfmetrixPackage.COURSE__NAME:
			return getName();
		case DiscgolfmetrixPackage.COURSE__BASKETS:
			return getBaskets();
		case DiscgolfmetrixPackage.COURSE__LAYOUTS:
			return getLayouts();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case DiscgolfmetrixPackage.COURSE__NAME:
			setName((String) newValue);
			return;
		case DiscgolfmetrixPackage.COURSE__BASKETS:
			setBaskets((Integer) newValue);
			return;
		case DiscgolfmetrixPackage.COURSE__LAYOUTS:
			getLayouts().clear();
			getLayouts().addAll((Collection<? extends CourseLayout>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case DiscgolfmetrixPackage.COURSE__NAME:
			setName(NAME_EDEFAULT);
			return;
		case DiscgolfmetrixPackage.COURSE__BASKETS:
			setBaskets(BASKETS_EDEFAULT);
			return;
		case DiscgolfmetrixPackage.COURSE__LAYOUTS:
			getLayouts().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case DiscgolfmetrixPackage.COURSE__NAME:
			return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		case DiscgolfmetrixPackage.COURSE__BASKETS:
			return BASKETS_EDEFAULT == null ? baskets != null : !BASKETS_EDEFAULT.equals(baskets);
		case DiscgolfmetrixPackage.COURSE__LAYOUTS:
			return layouts != null && !layouts.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", baskets: ");
		result.append(baskets);
		result.append(')');
		return result.toString();
	}

} //CourseImpl
