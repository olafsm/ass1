/**
 */
package tdt4250.discgolfmetrix.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

import tdt4250.discgolfmetrix.CourseLayout;
import tdt4250.discgolfmetrix.DiscgolfmetrixPackage;
import tdt4250.discgolfmetrix.Hole;
import tdt4250.discgolfmetrix.Par;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Hole</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.discgolfmetrix.impl.HoleImpl#getPar <em>Par</em>}</li>
 *   <li>{@link tdt4250.discgolfmetrix.impl.HoleImpl#getHoleNumber <em>Hole Number</em>}</li>
 *   <li>{@link tdt4250.discgolfmetrix.impl.HoleImpl#getCourseLayout <em>Course Layout</em>}</li>
 *   <li>{@link tdt4250.discgolfmetrix.impl.HoleImpl#getDistanceInMeters <em>Distance In Meters</em>}</li>
 *   <li>{@link tdt4250.discgolfmetrix.impl.HoleImpl#getDistanceInFeet <em>Distance In Feet</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HoleImpl extends MinimalEObjectImpl.Container implements Hole {
	/**
	 * The default value of the '{@link #getPar() <em>Par</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPar()
	 * @generated
	 * @ordered
	 */
	protected static final Par PAR_EDEFAULT = Par.PAR2;

	/**
	 * The cached value of the '{@link #getPar() <em>Par</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPar()
	 * @generated
	 * @ordered
	 */
	protected Par par = PAR_EDEFAULT;

	/**
	 * The default value of the '{@link #getHoleNumber() <em>Hole Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHoleNumber()
	 * @generated
	 * @ordered
	 */
	protected static final Integer HOLE_NUMBER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getHoleNumber() <em>Hole Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHoleNumber()
	 * @generated
	 * @ordered
	 */
	protected Integer holeNumber = HOLE_NUMBER_EDEFAULT;

	/**
	 * The default value of the '{@link #getDistanceInMeters() <em>Distance In Meters</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDistanceInMeters()
	 * @generated
	 * @ordered
	 */
	protected static final Integer DISTANCE_IN_METERS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDistanceInMeters() <em>Distance In Meters</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDistanceInMeters()
	 * @generated
	 * @ordered
	 */
	protected Integer distanceInMeters = DISTANCE_IN_METERS_EDEFAULT;

	/**
	 * The default value of the '{@link #getDistanceInFeet() <em>Distance In Feet</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDistanceInFeet()
	 * @generated
	 * @ordered
	 */
	protected static final Integer DISTANCE_IN_FEET_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDistanceInFeet() <em>Distance In Feet</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDistanceInFeet()
	 * @generated
	 * @ordered
	 */
	protected Integer distanceInFeet = DISTANCE_IN_FEET_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HoleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DiscgolfmetrixPackage.Literals.HOLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Par getPar() {
		return par;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPar(Par newPar) {
		Par oldPar = par;
		par = newPar == null ? PAR_EDEFAULT : newPar;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DiscgolfmetrixPackage.HOLE__PAR, oldPar, par));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integer getHoleNumber() {
		return holeNumber;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHoleNumber(Integer newHoleNumber) {
		Integer oldHoleNumber = holeNumber;
		holeNumber = newHoleNumber;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DiscgolfmetrixPackage.HOLE__HOLE_NUMBER,
					oldHoleNumber, holeNumber));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CourseLayout getCourseLayout() {
		if (eContainerFeatureID() != DiscgolfmetrixPackage.HOLE__COURSE_LAYOUT)
			return null;
		return (CourseLayout) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCourseLayout(CourseLayout newCourseLayout, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newCourseLayout, DiscgolfmetrixPackage.HOLE__COURSE_LAYOUT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCourseLayout(CourseLayout newCourseLayout) {
		if (newCourseLayout != eInternalContainer()
				|| (eContainerFeatureID() != DiscgolfmetrixPackage.HOLE__COURSE_LAYOUT && newCourseLayout != null)) {
			if (EcoreUtil.isAncestor(this, newCourseLayout))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newCourseLayout != null)
				msgs = ((InternalEObject) newCourseLayout).eInverseAdd(this, DiscgolfmetrixPackage.COURSE_LAYOUT__HOLE,
						CourseLayout.class, msgs);
			msgs = basicSetCourseLayout(newCourseLayout, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DiscgolfmetrixPackage.HOLE__COURSE_LAYOUT,
					newCourseLayout, newCourseLayout));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integer getDistanceInMeters() {
		return distanceInMeters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDistanceInMeters(Integer newDistanceInMeters) {
		Integer oldDistanceInMeters = distanceInMeters;
		distanceInMeters = newDistanceInMeters;
		distanceInFeet = (int) Math.floor(distanceInMeters*3.2808);
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DiscgolfmetrixPackage.HOLE__DISTANCE_IN_METERS,
					oldDistanceInMeters, distanceInMeters));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integer getDistanceInFeet() {
		return distanceInFeet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDistanceInFeet(Integer newDistanceInFeet) {
		Integer oldDistanceInFeet = distanceInFeet;
		distanceInFeet = newDistanceInFeet;
		distanceInMeters = (int) Math.floor(newDistanceInFeet/3.2808);
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DiscgolfmetrixPackage.HOLE__DISTANCE_IN_FEET,
					oldDistanceInFeet, distanceInFeet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case DiscgolfmetrixPackage.HOLE__COURSE_LAYOUT:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetCourseLayout((CourseLayout) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case DiscgolfmetrixPackage.HOLE__COURSE_LAYOUT:
			return basicSetCourseLayout(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case DiscgolfmetrixPackage.HOLE__COURSE_LAYOUT:
			return eInternalContainer().eInverseRemove(this, DiscgolfmetrixPackage.COURSE_LAYOUT__HOLE,
					CourseLayout.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case DiscgolfmetrixPackage.HOLE__PAR:
			return getPar();
		case DiscgolfmetrixPackage.HOLE__HOLE_NUMBER:
			return getHoleNumber();
		case DiscgolfmetrixPackage.HOLE__COURSE_LAYOUT:
			return getCourseLayout();
		case DiscgolfmetrixPackage.HOLE__DISTANCE_IN_METERS:
			return getDistanceInMeters();
		case DiscgolfmetrixPackage.HOLE__DISTANCE_IN_FEET:
			return getDistanceInFeet();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case DiscgolfmetrixPackage.HOLE__PAR:
			setPar((Par) newValue);
			return;
		case DiscgolfmetrixPackage.HOLE__HOLE_NUMBER:
			setHoleNumber((Integer) newValue);
			return;
		case DiscgolfmetrixPackage.HOLE__COURSE_LAYOUT:
			setCourseLayout((CourseLayout) newValue);
			return;
		case DiscgolfmetrixPackage.HOLE__DISTANCE_IN_METERS:
			setDistanceInMeters((Integer) newValue);
			return;
		case DiscgolfmetrixPackage.HOLE__DISTANCE_IN_FEET:
			setDistanceInFeet((Integer) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case DiscgolfmetrixPackage.HOLE__PAR:
			setPar(PAR_EDEFAULT);
			return;
		case DiscgolfmetrixPackage.HOLE__HOLE_NUMBER:
			setHoleNumber(HOLE_NUMBER_EDEFAULT);
			return;
		case DiscgolfmetrixPackage.HOLE__COURSE_LAYOUT:
			setCourseLayout((CourseLayout) null);
			return;
		case DiscgolfmetrixPackage.HOLE__DISTANCE_IN_METERS:
			setDistanceInMeters(DISTANCE_IN_METERS_EDEFAULT);
			return;
		case DiscgolfmetrixPackage.HOLE__DISTANCE_IN_FEET:
			setDistanceInFeet(DISTANCE_IN_FEET_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case DiscgolfmetrixPackage.HOLE__PAR:
			return par != PAR_EDEFAULT;
		case DiscgolfmetrixPackage.HOLE__HOLE_NUMBER:
			return HOLE_NUMBER_EDEFAULT == null ? holeNumber != null : !HOLE_NUMBER_EDEFAULT.equals(holeNumber);
		case DiscgolfmetrixPackage.HOLE__COURSE_LAYOUT:
			return getCourseLayout() != null;
		case DiscgolfmetrixPackage.HOLE__DISTANCE_IN_METERS:
			return DISTANCE_IN_METERS_EDEFAULT == null ? distanceInMeters != null
					: !DISTANCE_IN_METERS_EDEFAULT.equals(distanceInMeters);
		case DiscgolfmetrixPackage.HOLE__DISTANCE_IN_FEET:
			return DISTANCE_IN_FEET_EDEFAULT == null ? distanceInFeet != null
					: !DISTANCE_IN_FEET_EDEFAULT.equals(distanceInFeet);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (par: ");
		result.append(par);
		result.append(", holeNumber: ");
		result.append(holeNumber);
		result.append(", distanceInMeters: ");
		result.append(distanceInMeters);
		result.append(", distanceInFeet: ");
		result.append(distanceInFeet);
		result.append(')');
		return result.toString();
	}

} //HoleImpl
