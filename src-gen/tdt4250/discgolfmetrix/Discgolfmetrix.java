/**
 */
package tdt4250.discgolfmetrix;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Discgolfmetrix</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.discgolfmetrix.Discgolfmetrix#getUser <em>User</em>}</li>
 *   <li>{@link tdt4250.discgolfmetrix.Discgolfmetrix#getCourse <em>Course</em>}</li>
 *   <li>{@link tdt4250.discgolfmetrix.Discgolfmetrix#getClub <em>Club</em>}</li>
 *   <li>{@link tdt4250.discgolfmetrix.Discgolfmetrix#getEvent <em>Event</em>}</li>
 * </ul>
 *
 * @see tdt4250.discgolfmetrix.DiscgolfmetrixPackage#getDiscgolfmetrix()
 * @model
 * @generated
 */
public interface Discgolfmetrix extends EObject {
	/**
	 * Returns the value of the '<em><b>User</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>User</em>' containment reference.
	 * @see #setUser(User)
	 * @see tdt4250.discgolfmetrix.DiscgolfmetrixPackage#getDiscgolfmetrix_User()
	 * @model containment="true"
	 * @generated
	 */
	User getUser();

	/**
	 * Sets the value of the '{@link tdt4250.discgolfmetrix.Discgolfmetrix#getUser <em>User</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>User</em>' containment reference.
	 * @see #getUser()
	 * @generated
	 */
	void setUser(User value);

	/**
	 * Returns the value of the '<em><b>Course</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Course</em>' containment reference.
	 * @see #setCourse(Course)
	 * @see tdt4250.discgolfmetrix.DiscgolfmetrixPackage#getDiscgolfmetrix_Course()
	 * @model containment="true"
	 * @generated
	 */
	Course getCourse();

	/**
	 * Sets the value of the '{@link tdt4250.discgolfmetrix.Discgolfmetrix#getCourse <em>Course</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Course</em>' containment reference.
	 * @see #getCourse()
	 * @generated
	 */
	void setCourse(Course value);

	/**
	 * Returns the value of the '<em><b>Club</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Club</em>' containment reference.
	 * @see #setClub(Club)
	 * @see tdt4250.discgolfmetrix.DiscgolfmetrixPackage#getDiscgolfmetrix_Club()
	 * @model containment="true"
	 * @generated
	 */
	Club getClub();

	/**
	 * Sets the value of the '{@link tdt4250.discgolfmetrix.Discgolfmetrix#getClub <em>Club</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Club</em>' containment reference.
	 * @see #getClub()
	 * @generated
	 */
	void setClub(Club value);

	/**
	 * Returns the value of the '<em><b>Event</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event</em>' containment reference.
	 * @see #setEvent(Event)
	 * @see tdt4250.discgolfmetrix.DiscgolfmetrixPackage#getDiscgolfmetrix_Event()
	 * @model containment="true"
	 * @generated
	 */
	Event getEvent();

	/**
	 * Sets the value of the '{@link tdt4250.discgolfmetrix.Discgolfmetrix#getEvent <em>Event</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Event</em>' containment reference.
	 * @see #getEvent()
	 * @generated
	 */
	void setEvent(Event value);

} // Discgolfmetrix
