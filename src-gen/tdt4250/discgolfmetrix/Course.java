/**
 */
package tdt4250.discgolfmetrix;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Course</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.discgolfmetrix.Course#getName <em>Name</em>}</li>
 *   <li>{@link tdt4250.discgolfmetrix.Course#getBaskets <em>Baskets</em>}</li>
 *   <li>{@link tdt4250.discgolfmetrix.Course#getLayouts <em>Layouts</em>}</li>
 * </ul>
 *
 * @see tdt4250.discgolfmetrix.DiscgolfmetrixPackage#getCourse()
 * @model
 * @generated
 */
public interface Course extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see tdt4250.discgolfmetrix.DiscgolfmetrixPackage#getCourse_Name()
	 * @model id="true" required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link tdt4250.discgolfmetrix.Course#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Baskets</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Baskets</em>' attribute.
	 * @see #setBaskets(Integer)
	 * @see tdt4250.discgolfmetrix.DiscgolfmetrixPackage#getCourse_Baskets()
	 * @model unique="false" required="true"
	 * @generated
	 */
	Integer getBaskets();

	/**
	 * Sets the value of the '{@link tdt4250.discgolfmetrix.Course#getBaskets <em>Baskets</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Baskets</em>' attribute.
	 * @see #getBaskets()
	 * @generated
	 */
	void setBaskets(Integer value);

	/**
	 * Returns the value of the '<em><b>Layouts</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250.discgolfmetrix.CourseLayout}.
	 * It is bidirectional and its opposite is '{@link tdt4250.discgolfmetrix.CourseLayout#getCourse <em>Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Layouts</em>' containment reference list.
	 * @see tdt4250.discgolfmetrix.DiscgolfmetrixPackage#getCourse_Layouts()
	 * @see tdt4250.discgolfmetrix.CourseLayout#getCourse
	 * @model opposite="course" containment="true" required="true"
	 * @generated
	 */
	EList<CourseLayout> getLayouts();

} // Course
