/**
 */
package tdt4250.discgolfmetrix;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>User</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.discgolfmetrix.User#getFirstname <em>Firstname</em>}</li>
 *   <li>{@link tdt4250.discgolfmetrix.User#getLastname <em>Lastname</em>}</li>
 *   <li>{@link tdt4250.discgolfmetrix.User#getName <em>Name</em>}</li>
 *   <li>{@link tdt4250.discgolfmetrix.User#getNickname <em>Nickname</em>}</li>
 *   <li>{@link tdt4250.discgolfmetrix.User#getEmail <em>Email</em>}</li>
 *   <li>{@link tdt4250.discgolfmetrix.User#getPDGAnumber <em>PDG Anumber</em>}</li>
 *   <li>{@link tdt4250.discgolfmetrix.User#getUserID <em>User ID</em>}</li>
 *   <li>{@link tdt4250.discgolfmetrix.User#getRating <em>Rating</em>}</li>
 *   <li>{@link tdt4250.discgolfmetrix.User#getClub <em>Club</em>}</li>
 *   <li>{@link tdt4250.discgolfmetrix.User#getParticipant <em>Participant</em>}</li>
 *   <li>{@link tdt4250.discgolfmetrix.User#getCoordinator <em>Coordinator</em>}</li>
 * </ul>
 *
 * @see tdt4250.discgolfmetrix.DiscgolfmetrixPackage#getUser()
 * @model
 * @generated
 */
public interface User extends EObject {
	/**
	 * Returns the value of the '<em><b>Firstname</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Firstname</em>' attribute.
	 * @see #setFirstname(String)
	 * @see tdt4250.discgolfmetrix.DiscgolfmetrixPackage#getUser_Firstname()
	 * @model unique="false" required="true"
	 * @generated
	 */
	String getFirstname();

	/**
	 * Sets the value of the '{@link tdt4250.discgolfmetrix.User#getFirstname <em>Firstname</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Firstname</em>' attribute.
	 * @see #getFirstname()
	 * @generated
	 */
	void setFirstname(String value);

	/**
	 * Returns the value of the '<em><b>Lastname</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lastname</em>' attribute.
	 * @see #setLastname(String)
	 * @see tdt4250.discgolfmetrix.DiscgolfmetrixPackage#getUser_Lastname()
	 * @model unique="false" required="true"
	 * @generated
	 */
	String getLastname();

	/**
	 * Sets the value of the '{@link tdt4250.discgolfmetrix.User#getLastname <em>Lastname</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lastname</em>' attribute.
	 * @see #getLastname()
	 * @generated
	 */
	void setLastname(String value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #isSetName()
	 * @see tdt4250.discgolfmetrix.DiscgolfmetrixPackage#getUser_Name()
	 * @model unique="false" unsettable="true" required="true" transient="true" changeable="false" derived="true"
	 * @generated
	 */
	String getName();

	/**
	 * Returns whether the value of the '{@link tdt4250.discgolfmetrix.User#getName <em>Name</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Name</em>' attribute is set.
	 * @see #getName()
	 * @generated
	 */
	boolean isSetName();

	/**
	 * Returns the value of the '<em><b>Nickname</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nickname</em>' attribute.
	 * @see #setNickname(String)
	 * @see tdt4250.discgolfmetrix.DiscgolfmetrixPackage#getUser_Nickname()
	 * @model required="true"
	 * @generated
	 */
	String getNickname();

	/**
	 * Sets the value of the '{@link tdt4250.discgolfmetrix.User#getNickname <em>Nickname</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Nickname</em>' attribute.
	 * @see #getNickname()
	 * @generated
	 */
	void setNickname(String value);

	/**
	 * Returns the value of the '<em><b>Email</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Email</em>' attribute.
	 * @see #setEmail(String)
	 * @see tdt4250.discgolfmetrix.DiscgolfmetrixPackage#getUser_Email()
	 * @model required="true"
	 * @generated
	 */
	String getEmail();

	/**
	 * Sets the value of the '{@link tdt4250.discgolfmetrix.User#getEmail <em>Email</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Email</em>' attribute.
	 * @see #getEmail()
	 * @generated
	 */
	void setEmail(String value);

	/**
	 * Returns the value of the '<em><b>PDG Anumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>PDG Anumber</em>' attribute.
	 * @see #setPDGAnumber(Integer)
	 * @see tdt4250.discgolfmetrix.DiscgolfmetrixPackage#getUser_PDGAnumber()
	 * @model
	 * @generated
	 */
	Integer getPDGAnumber();

	/**
	 * Sets the value of the '{@link tdt4250.discgolfmetrix.User#getPDGAnumber <em>PDG Anumber</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>PDG Anumber</em>' attribute.
	 * @see #getPDGAnumber()
	 * @generated
	 */
	void setPDGAnumber(Integer value);

	/**
	 * Returns the value of the '<em><b>User ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>User ID</em>' attribute.
	 * @see #setUserID(Integer)
	 * @see tdt4250.discgolfmetrix.DiscgolfmetrixPackage#getUser_UserID()
	 * @model id="true" required="true"
	 * @generated
	 */
	Integer getUserID();

	/**
	 * Sets the value of the '{@link tdt4250.discgolfmetrix.User#getUserID <em>User ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>User ID</em>' attribute.
	 * @see #getUserID()
	 * @generated
	 */
	void setUserID(Integer value);

	/**
	 * Returns the value of the '<em><b>Rating</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rating</em>' attribute.
	 * @see #setRating(Integer)
	 * @see tdt4250.discgolfmetrix.DiscgolfmetrixPackage#getUser_Rating()
	 * @model unique="false"
	 * @generated
	 */
	Integer getRating();

	/**
	 * Sets the value of the '{@link tdt4250.discgolfmetrix.User#getRating <em>Rating</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rating</em>' attribute.
	 * @see #getRating()
	 * @generated
	 */
	void setRating(Integer value);

	/**
	 * Returns the value of the '<em><b>Club</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link tdt4250.discgolfmetrix.Club#getMember <em>Member</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Club</em>' container reference.
	 * @see #setClub(Club)
	 * @see tdt4250.discgolfmetrix.DiscgolfmetrixPackage#getUser_Club()
	 * @see tdt4250.discgolfmetrix.Club#getMember
	 * @model opposite="member" transient="false"
	 * @generated
	 */
	Club getClub();

	/**
	 * Sets the value of the '{@link tdt4250.discgolfmetrix.User#getClub <em>Club</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Club</em>' container reference.
	 * @see #getClub()
	 * @generated
	 */
	void setClub(Club value);

	/**
	 * Returns the value of the '<em><b>Participant</b></em>' reference list.
	 * The list contents are of type {@link tdt4250.discgolfmetrix.Event}.
	 * It is bidirectional and its opposite is '{@link tdt4250.discgolfmetrix.Event#getParticipant <em>Participant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Participant</em>' reference list.
	 * @see tdt4250.discgolfmetrix.DiscgolfmetrixPackage#getUser_Participant()
	 * @see tdt4250.discgolfmetrix.Event#getParticipant
	 * @model opposite="participant"
	 * @generated
	 */
	EList<Event> getParticipant();

	/**
	 * Returns the value of the '<em><b>Coordinator</b></em>' reference list.
	 * The list contents are of type {@link tdt4250.discgolfmetrix.Event}.
	 * It is bidirectional and its opposite is '{@link tdt4250.discgolfmetrix.Event#getCoordinator <em>Coordinator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Coordinator</em>' reference list.
	 * @see tdt4250.discgolfmetrix.DiscgolfmetrixPackage#getUser_Coordinator()
	 * @see tdt4250.discgolfmetrix.Event#getCoordinator
	 * @model opposite="coordinator"
	 * @generated
	 */
	EList<Event> getCoordinator();

} // User
